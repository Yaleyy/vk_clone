import 'package:flutter/material.dart';
import 'package:vk_app/Theme/text_theme.dart';
import 'package:vk_app/pages/for_you_page.dart';
import 'package:vk_app/pages/news_page.dart';
import 'package:vk_app/tabbar_list/tape.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: Colors.grey.shade100,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          leading: IconButton(
            splashRadius: 25,
            onPressed: () {},
            icon: Icon(
              Icons.account_circle_rounded,
              color: Colors.black.withOpacity(0.7),
              size: 30,
            ),
          ),
          title: Text(
            'Главная',
            style: TextMain.style,
          ),
          actions: [
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.add_circle_outline,
                size: 25,
                color: Colors.blueAccent.shade200,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.search,
                size: 25,
                color: Colors.blueAccent.shade200,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.notifications,
                size: 25,
                color: Colors.blueAccent.shade200,
              ),
            ),
          ],
          bottom: TabBar(
            labelStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
            unselectedLabelColor: Colors.grey,
            labelColor: Colors.black,
            tabs: [
              Tab(
                text: 'Лента',
              ),
              Tab(
                text: 'Для вас',
              ),
              Tab(
                text: 'Новости',
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            TapeWidget(),
            ForYouWidget(),
            NewsPage(),
          ],
        ),
        bottomNavigationBar: Container(
          decoration: BoxDecoration(
            border: Border(top: BorderSide(color: Colors.grey.withOpacity(0.1)))
          ),
          height: 60,
          child: BottomNavigationBar(
            currentIndex: index,
            onTap: (value) {
              setState(() {
                index = value;
              });
            },
            selectedLabelStyle: TextStyle(
              fontSize: 12,
            ),
            selectedItemColor: Colors.blueAccent.shade400,
            selectedIconTheme: IconThemeData(color: Colors.blueAccent.shade400),
            type: BottomNavigationBarType.fixed,
            showUnselectedLabels: true,

            items: [
              BottomNavigationBarItem(
                icon: Transform.scale(
                  scale: 1.4,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Image.asset(
                      'assets/home.png',
                      color: index == 0 ? Colors.blueAccent.shade400 : Colors.grey,
                    ),
                  ),
                ),
                label: 'Главная',
              ),
              BottomNavigationBarItem(
                icon: Transform.scale(
                  scale: 1.4,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Image.asset(
                      'assets/servis.png',
                      color: index == 1 ? Colors.blueAccent.shade400 : Colors.grey,
                    ),
                  ),
                ),
                label: 'Сервисы',
              ),
              BottomNavigationBarItem(
                icon: Transform.scale(
                  scale: 1.4,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Image.asset(
                      'assets/sms.png',
                      color: index == 2 ? Colors.blueAccent.shade400 : Colors.grey,
                    ),
                  ),
                ),
                label: 'Мессенджер',
              ),
              BottomNavigationBarItem(
                icon: Transform.scale(
                  scale: 1.4,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Image.asset(
                      'assets/clips.png',
                      color: index == 3 ? Colors.blueAccent.shade400 : Colors.grey,
                    ),
                  ),
                ),
                label: 'Клипы',
              ),
              BottomNavigationBarItem(
                icon: Transform.scale(
                  scale: 1.4,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Image.asset(
                      'assets/video.png',
                      color: index == 4 ? Colors.blueAccent.shade400 : Colors.grey,
                    ),
                  ),
                ),
                label: 'Видео',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
