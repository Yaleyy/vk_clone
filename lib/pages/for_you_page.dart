import 'package:flutter/material.dart';

class ForYouWidget extends StatelessWidget {
  const ForYouWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3
          ),
          itemCount: 20,
          itemBuilder: (context, index){
            return Container(
              alignment: Alignment.center,
              margin: EdgeInsets.all(0.6),
              height: 200,
              width: 200,
              color: index % 2 == 0 ? Colors.green.shade300 : Colors.orange.shade400,
              child: Text('Запись $index'),
            );
          },
      ),
    );
  }
}
