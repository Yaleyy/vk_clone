import 'package:flutter/material.dart';

class NewsPage extends StatefulWidget {
  NewsPage({super.key});

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  List<Widget> textBottonWidgets = [
    TextButtonWidget(
      textNews: 'Сегодня в мире произошло то-то, то-то\nСобытия развиваются так-то, так-то',
      color: Colors.red,
    ),
    TextButtonWidget(
      textNews: 'Такео-то, такое-то событие\nпроизшло прошлой ночью',
      color: Colors.blue,
    ),
    TextButtonWidget(
      textNews: 'Тот-то, тот-то подписал указ такой-то, такой-то',
      color: Colors.orange,
    ),
    TextButtonWidget(
      textNews: 'Кто-то сказал так-то, так-то',
      color: Colors.green,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const SizedBox(width: 12),
                    Icon(Icons.newspaper, color: Colors.black.withOpacity(0.7), size: 25,),
                    const SizedBox(width: 10),
                    const Text('Сейчас в СМИ', style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                for (int i = 0; i < textBottonWidgets.length; i++)
                  textBottonWidgets[i],

                textBottonWidgets.length <= 6 ? TextButton(
                  onPressed: () {
                    setState(() {
                      textBottonWidgets.add(
                        TextButtonWidget(
                          textNews: 'Ещё одна новость',
                          color: Colors.black,
                        ),
                      );
                    },);
                  },
                  child: const Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.expand_more_outlined, color: Colors.blue,),
                      SizedBox(width: 15),
                      Text('Показать ещё'),
                    ],
                  ),
                ) :
                TextButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context){
                        return AlertDialog(
                          alignment: Alignment.bottomCenter,
                          content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Icon(Icons.info, size: 50,),
                              Text('Новостной блок\nООО "ДЗЕН ПЛАТФОРМА":подборка материалов и их порядок регулируется\nисключительно сервисом',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 13),
                              ),
                              Container(
                                width: double.infinity,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    elevation: 0,
                                    backgroundColor: Colors.grey.shade100,
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text('Понятно', style: TextStyle(color: Colors.blueAccent.shade400),),
                                ),
                              )
                            ],
                          )
                        );
                      },
                    );
                  },
                  child: const Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.info, color: Colors.grey,),
                      SizedBox(width: 15),
                      Text(
                        'Подборка Дзена',
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 5),
          NewsWidget(),
          SizedBox(height: 5),
          NewsWidget(),
          SizedBox(height: 5),
          NewsWidget(),
          SizedBox(height: 5),
          NewsWidget(),
          SizedBox(height: 5),
          NewsWidget(),
        ],
      ),
    );
  }
}

class NewsWidget extends StatelessWidget {
  const NewsWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            children: [
              Text('Название новости', style: TextStyle(fontSize: 17),),
            ],
          ),
          SizedBox(height: 10),
          Container(
            alignment: Alignment.center,
            height: 200,
            width: double.infinity,
            color: Colors.black.withOpacity(0.3),
            child: Text('Фото новости'),
          ),
          TextButton(
            onPressed: () {},
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Показать все новости', style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 18
                ),),
                Icon(Icons.chevron_right, size: 23,)
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class TextButtonWidget extends StatelessWidget {
  String textNews;
  Color color;
  TextButtonWidget({
    super.key, required this.textNews, required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () {},
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Icon(Icons.circle, color: color, size: 30,),
            const SizedBox(width: 10),
            Text(textNews,
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w400,
              ),
            ),
          ],
        ),
    );
  }
}
